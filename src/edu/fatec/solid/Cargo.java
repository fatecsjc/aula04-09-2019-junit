package edu.fatec.solid;

public enum Cargo {
	DESENVOLVEDOR(new RegraDezouVinte()), 
	DBA(new RegraQuinzeouVinteCinco()), 
	TESTER(new RegraQuinzeouVinteCinco());
	
	RegraPorcentagem regraPorcentagem;
	
	Cargo(RegraPorcentagem r) {
		regraPorcentagem = r;
	}
	
	public RegraPorcentagem getRegraPorcentagem() {
		return regraPorcentagem;
	}
}

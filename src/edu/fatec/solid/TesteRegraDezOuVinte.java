package edu.fatec.solid;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TesteRegraDezOuVinte {
	
	
	private RegraDezOuVinte regraDezouVinte;
	private Funcionario<?> funcionario;
	
	@Before
	public void init() {
		
		regraDezouVinte = new RegraDezOuVinte();
		funcionario = new Funcionario<>();
	}
	
	@Test
	public void validaFator80(RegraPorcentagem r) {
		
		assertEquals(900.0, regraDezouVinte.calcular(funcionario), 1);
	}
}

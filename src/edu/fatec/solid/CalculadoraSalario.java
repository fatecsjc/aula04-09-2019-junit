package edu.fatec.solid;

public class CalculadoraSalario {
	public double calcula(Funcionario funcionario) {
		if (funcionario != null && funcionario.getCargo() != null) {
			return funcionario.getCargo()
					.getRegraPorcentagem()
					.calcular(funcionario);
		}

		throw new RuntimeException("funcionario invalido");
	}

}
